const fs = require('fs')
const path = require('path')

const data = fs.readFileSync(path.resolve(__dirname, 'result.json'))
const parsedData = JSON.parse(data)
let result = {}

const categoriesKeys = Object.keys(parsedData.categories)
for(let key of categoriesKeys) {
  result[key] = parseInt((parsedData.categories[key]).score * 100)
}

// add metrices
const metrics = parsedData.audits.metrics.details.items[0]
const metricsKeys = Object.keys(metrics)
for(let key of metricsKeys) {
  if(!key.includes("observed")) {
    result[key] = metrics[key]
  }
}

console.log(parsedData.categories['best-practices'])
