FROM ubuntu:16.04
RUN apt-get update -y
RUN apt-get install curl -y

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && apt-get install -y nodejs
# RUN globally dependency
RUN npm install -g firebase-tools
RUN npm install -g lighthouse
RUN apt-get install -y chromium-browser
RUN apt-get install -f

# Create app directory
WORKDIR /usr/src/app


# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .


# BUILD for production
RUN npm run build


RUN firebase deploy --token "1/xaj3QzhqI1nUtXbAx3g6s2eeDuXoEXhjURbT24BtBUI"

# build files deploy status
RUN lighthouse --chrome-flags="--headless --no-sandbox" https://opscale-demo-e423a.firebaseapp.com --output=json > result.json
RUN node sendreport.js
